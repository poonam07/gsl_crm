### Project overview###

# Git
```git clone https://gitlab.com/atul4113/gsl_crm.git```

Note: it's a private repository, password may be required.


####################################################################################################################################################################


# Our project objective

This application allows users to create projects with budget, description of work and its
duration, schedule, Location and contact details in the system. The application will work in
online mode user can insert new record (create project, notes and tracking photos and
videos) which is uploaded. There would be various user roles in the system as per the access
rights. The access rights would vary on the basis of projects.

## Project structure (https://gitlab.com/atul4113/gsl_crm)

* [accounts](#)
* [admin](#)
* [api](#)
* [contractors](#)
* [sub contractors](#)
* [customers](#)
* [orders](#)
* [project managers](#)
* [suppliers](#)
* [venders](#)


```
GSLCRM/
├── accounts/		# All our accounts
│   └── __init__.py
│   └── ....
├── admin/		# Admin controls
│   └── __init__.py
│   └── ....
├── api/		# API
│   └── __init__.py
│   └── ....
├── contractors/	# All our contractors
│   └── __init__.py
│   └── ....
├── customers/		# All our customers
│   └── __init__.py
│   └── ....
├── orders/		# All orders
│   └── __init__.py
│   └── ....
├── project_managers/	# All project managers
│   └── __init__.py
│   └── ....
├── sub_contractors/	# All sub contractors
│   └── __init__.py
│   └── ....
├── suppliers/		# All suppliers
│   └── __init__.py
│   └── ....
├── venders/		# All venders
│   └── __init__.py
│   └── ....
├── manage.py        		# Django manager
├── ../gitignore		# Our git ignore file
├── ../README.md		# Read me for basic instructions
└── ../requirements.txt      	# Required package list

```

## API access url

* [customer](http://127.0.0.1:8000/api/v1/customers)``` http://127.0.0.1:8000/api/v1/customers ```


## More about project

Get every updates of flask news sentiments API

- Read here [Read more](#).

## Copyright and license

copyright 2020 Global Soft Labs Pvt Ltd


