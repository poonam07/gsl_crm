# Generated by Django 2.2 on 2020-06-03 19:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('project_total', models.PositiveIntegerField(blank=True, help_text='Project Total Amount', null=True)),
                ('current_balance', models.PositiveIntegerField(blank=True, help_text='Current Balance', null=True)),
                ('amount_spent', models.PositiveIntegerField(blank=True, help_text='Amount Spent', null=True)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='project_status', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
