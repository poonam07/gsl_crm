from django.db import models
from account.models import User


class ProjectStatus(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='project_status')
    project_total = models.PositiveIntegerField(blank=True, null=True, help_text='Project Total Amount')
    current_balance = models.PositiveIntegerField(blank=True, null=True, help_text='Current Balance')
    amount_spent = models.PositiveIntegerField(blank=True, null=True, help_text='Amount Spent')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.full_name)



