from django.contrib import admin

from .models import (Customer, CustomerAddress, CustomerAccount, CustomerDocument)

admin.site.register(Customer)
admin.site.register(CustomerAddress)
admin.site.register(CustomerAccount)
admin.site.register(CustomerDocument)