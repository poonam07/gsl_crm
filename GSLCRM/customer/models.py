from django.db import models
from account.models import User
from django.core.validators import (FileExtensionValidator, RegexValidator)


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customers')
    phone = models.CharField(max_length=10, db_index=True,
                             validators=[RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                                        message='Enter a valid phone number')], unique=True)

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.full_name)


class CustomerAddress(models.Model):
    customer = models.OneToOneField(Customer, on_delete=models.CASCADE, related_name='customer_address')
    street = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address')
    street_2 = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address 2')
    city = models.CharField(max_length=250, blank=True, null=True, help_text='Your City')
    state = models.CharField(max_length=250, blank=True, null=True, help_text='Your State')
    country = models.CharField(max_length=250, blank=True, null=True, help_text='Your Country will be shared.')
    zip = models.PositiveIntegerField(blank=True, null=True, help_text='Your zip')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.customer)


class CustomerAccount(models.Model):
    customer = models.OneToOneField(Customer, on_delete=models.CASCADE, related_name='customer_accounts')
    project_total = models.PositiveIntegerField(blank=True, null=True, help_text='Project Total Amount')
    current_balance = models.PositiveIntegerField(blank=True, null=True, help_text='Current Balance')
    amount_spent = models.PositiveIntegerField(blank=True, null=True, help_text='Amount Spent')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.customer)


class CustomerDocument(models.Model):
    customer = models.OneToOneField(Customer, on_delete=models.CASCADE, related_name='customer_documents')
    # file = models.ImageField(upload_to='customer/documents/%Y/%m/%d/', blank=True,
    #                          validators=[validate_file_extension],
    #                          help_text='Max size 20MB, and only pdf are allowed')
    file = models.FileField(upload_to='customer/documents/%Y/%m/%d/', blank=True,
                             validators=[FileExtensionValidator(allowed_extensions=['pdf','PDF'])],
                             help_text='Max size 20MB, and only pdf are allowed')
    description = models.TextField(max_length=10000, help_text='Description - max 10000 words')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.customer)

