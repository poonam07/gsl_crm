from django.apps import AppConfig


class SubContractorsConfig(AppConfig):
    name = 'sub_contractor'
