from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from rest_framework_swagger.views import get_swagger_view
from .routers import router
from api.viewsets import LoginView, LogoutView, UserLoginAPIView

schema_view = get_swagger_view(title="API Documentation")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/', include(router.urls)),
    path('api/docs/', schema_view),
    # path('api/v1/auth/login/', LoginView.as_view()),
    # path('api/v1/auth/logout/', LogoutView.as_view()),
    url(r'^api/v1/auth/login/$', UserLoginAPIView.as_view(), name='api_login'),
    url(r'^api/v1/auth/social-login/', include('rest_social_auth.urls_session')),
    # url('social', include('social_django.urls', namespace='social')),
    # app urls
    url('^account/', include('account.urls')),
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# error handler
# handler400 = views.error_400
# handler403 = views.error_403
# handler404 = views.error_404
# handler413 = views.error_413
# handler500 = views.error_500

