import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'cwmxrdf%*xvw9%r@gi$^oamni*j)lw4g4hsl576qu!ti-xjdi$'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# DEBUG = False

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # third party apps
    'corsheaders',
    'social_django',
    'rest_social_auth',
    'sorl.thumbnail',
    'crispy_forms',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_tracking',
    'rest_framework_swagger',
    # our apps
    'account',
    'billing',
    'contractor',
    'customer',
    'manager',
    'order',
    'project_manager',
    'project',
    'report',
    'sub_contractor',
    'supplier',
    'vendor'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'GSLCRM.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

WSGI_APPLICATION = 'GSLCRM.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'new_db.sqlite3'),
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': 'amritsaagar',
#         'USER': 'amritsaagar',
#         # 'USER': 'postgres',
#         'PASSWORD': 'atul4113',
#         # 'HOST': 'amritsaagar.com',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
#  }

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

# CSRF_COOKIE_SECURE = True
# SESSION_COOKIE_SECURE = True
# X_FRAME_OPTIONS = 'DENY'
# SECURE_HSTS_SECONDS = True
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True
# SECURE_HSTS_PRELOAD = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_BROWSER_XSS_FILTER = True
# SECURE_SSL_REDIRECT = True

CRISPY_TEMPLATE_PACK = 'bootstrap4'

# #Email Config
# EMAIL_HOST = 'smtpout.secureserver.net'
# EMAIL_HOST_USER = 'salse@globalsoftlabs.com'
# EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', 'Salse@one23')
# EMAIL_PORT = 465
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = 'Global Soft Labs <salse@globalsoftlabs.com>'
# BASE_URL = 'http://www.flobrr.com'
# # BASE_URL = '127.0.0.1:8000'
#
# MANAGERS = (
#     ('Global Soft Labs Pvt Ltd', "salse@globalsoftlabs.com"),
#     ('GSL', "salse@globalsoftlabs.com")
# )
#Email Config
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'gslcrmtesting@gmail.com'
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', 'eqtcscanxwganhqa')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
# EMAIL_USE_SSL = True # ssl certificate
EMAIL_TIMEOUT = 300 # in seconds
DEFAULT_FROM_EMAIL = 'Global Soft Labs <salse@globalsoftlabs.com>'
BASE_URL = 'http://www.flobrr.com'
# BASE_URL = '127.0.0.1:8000'

MANAGERS = (
    ('Global Soft Labs Pvt Ltd', "salse@globalsoftlabs.com"),
    ('GSL', "salse@globalsoftlabs.com")
)

# LOGIN_URL = 'app:login'
# LOGIN_REDIRECT_URL = 'app:index'
LOGIN_REDIRECT_URL = 'logout'
LOGOUT_REDIRECT_URL = 'login'

REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0


CORS_ORIGIN_ALLOW_ALL = True
X_FRAME_OPTIONS = 'ALLOWALL'
AUTH_USER_MODEL = 'account.User'
# for postgresql
# SOCIAL_AUTH_POSTGRES_JSONFIELD = True

SOCIAL_AUTH_FACEBOOK_KEY = 'your app client id'
SOCIAL_AUTH_FACEBOOK_SECRET = 'your app client secret'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', ]  # optional

AUTHENTICATION_BACKENDS = (
    'social_core.backends.open_id.OpenIdAuth',
    'social_core.backends.google.GoogleOpenId',
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.google.GoogleOAuth',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.twitter.TwitterOAuth',
    'social_core.backends.yahoo.YahooOpenId',
    'django.contrib.auth.backends.ModelBackend',
)
SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
            # 'rest_framework.authentication.SessionAuthentication',
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework.authentication.TokenAuthentication',
        ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    # 'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    },
}