from rest_framework import routers

from api.viewsets import *

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
# router.register(r'users-registration', UserRegistrationViewSet)
# contractors
router.register(r'contractors', ContractorViewSet)
router.register(r'contractors-address', ContractorAddressViewSet)
router.register(r'contractors-service', ContractorServiceViewSet)
router.register(r'contractors-licence', ContractorLicenceViewSet)
router.register(r'contractors-review', ContractorReviewViewSet)
router.register(r'contractors-share', ContractorShareViewSet)
# customers
router.register(r'customers', CustomerViewSet)
router.register(r'customers-address', CustomerAddressViewSet)
router.register(r'customers-account', CustomerAccountViewSet)
router.register(r'customers-document', CustomerDocumentViewSet)
