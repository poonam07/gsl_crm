from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import (EmailActivation)

User = get_user_model()


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'mobile', 'first_name', 'customer', 'vendor', 'supplier', 'contractor',
                    'sub_contractor', 'project_manager', 'manager', 'is_active', 'created')
    list_display_links = ('id', 'email', )
    list_filter = ('first_name', 'customer', 'vendor', 'supplier', 'contractor', 'sub_contractor', 'project_manager',
                   'manager', 'is_active', 'created', )
    list_editable = ('customer', 'vendor', 'supplier', 'contractor', 'sub_contractor', 'project_manager', 'manager',
                     'is_active',)
    search_fields = ('mobile', 'first_name', 'email', 'customer', 'vendor', 'supplier', 'contractor', 'sub_contractor',
                     'project_manager', 'manager', 'is_active', 'created',)
    date_hierarchy = 'created'
    ordering = ('-created',)


class EmailActivationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'email', 'activated', 'timestamp')
    list_display_links = ('id', 'user', 'email',)
    list_editable = ('activated',)
    search_fields = ('email',)


admin.site.register(User, UserAdmin)
admin.site.register(EmailActivation, EmailActivationAdmin)

admin.site.site_header = "GSLCRM Administration"
admin.site.site_title = "Administration Portal"
admin.site.index_title = "Welcome to Admin Portal"

