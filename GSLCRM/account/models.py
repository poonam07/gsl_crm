from datetime import timedelta
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager)
from django.core.mail import send_mail
from django.template.loader import get_template
from django.utils import timezone
from django.core.validators import (EmailValidator, RegexValidator)


from GSLCRM.utils import unique_key_generator
# send_mail(subject, message, from_email, recipient_list, html_message)

DEFAULT_ACTIVATION_DAYS = getattr(settings, 'DEFAULT_ACTIVATION_DAYS', 7)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_active=False, is_staff=False, is_admin=False,
                    is_customer=True, is_vendor=False, is_supplier=False, is_contractor=False,
                    is_sub_contractor=False, is_project_manager=False, is_manager=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")
        user = self.model(
            email = self.normalize_email(email),
        )
        user.set_password(password)
        user.is_active = is_active
        user.staff = is_staff
        user.admin = is_admin
        user.customer = is_customer
        user.vendor = is_vendor
        user.supplier = is_supplier
        user.contractor = is_contractor
        user.sub_contractor = is_sub_contractor
        user.project_manager = is_project_manager
        user.manager = is_manager
        user.save(using=self._db)
        return user

    def create_customer(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_customer=True
        )
        return user

    def create_vendor(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_vendor=True
        )
        return user

    def create_supplier(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_supplier=True
        )
        return user

    def create_contractor(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_contractor=True
        )
        return user

    def create_sub_contractor(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_sub_contractor=True
        )
        return user

    def create_project_manager(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_project_manager=True
        )
        return user

    def create_manager(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_manager=True
        )
        return user

    def create_staffuser(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_staff=True,
                is_active=True,
                is_customer=True,
                is_vendor=True,
                is_supplier=True,
                is_contractor=True,
                is_sub_contractor=True,
                is_project_manager=True,
                is_manager=True
        )
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_staff=True,
                is_admin=True,
                is_active=True,
                is_customer=True,
                is_vendor=True,
                is_supplier=True,
                is_contractor=True,
                is_sub_contractor=True,
                is_project_manager=True,
                is_manager=True
        )
        return user


class User(AbstractBaseUser):
    email = models.EmailField(db_index=True, validators=[EmailValidator], unique=True)
    mobile = models.CharField(max_length=10, db_index=True,
                              validators=[RegexValidator(regex=r'^\+?1?\d{9,15}$', message='Enter a valid phone number')],
                              unique=True)
    first_name = models.CharField(max_length=250)
    middle_name = models.CharField(max_length=250, blank=True, null=True)
    last_name = models.CharField(max_length=250)

    customer = models.BooleanField(default=False)
    vendor = models.BooleanField(default=False)
    supplier = models.BooleanField(default=False)
    contractor = models.BooleanField(default=False)
    sub_contractor = models.BooleanField(default=False)
    project_manager = models.BooleanField(default=False)
    manager = models.BooleanField(default=False)

    is_active = models.BooleanField(default=False)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        if self.is_admin:
            return True
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_customer(self):
        return self.customer

    @property
    def is_vendor(self):
        return self.vendor

    @property
    def is_supplier(self):
        return self.supplier

    @property
    def is_contractor(self):
        return self.contractor

    @property
    def is_sub_contractor(self):
        return self.sub_contractor

    @property
    def is_project_manager(self):
        return self.project_manager

    @property
    def is_manager(self):
        return self.manager

    @property
    def full_name(self):
        if self.middle_name is None:
            return '%s %s' % (self.first_name, self.last_name)
        return '%s %s %s' % (self.first_name, self.middle_name, self.last_name)


class EmailActivationQuerySet(models.query.QuerySet):

    def to_be_confirm(self):
        now = timezone.now()
        start_range = now - timedelta(days=DEFAULT_ACTIVATION_DAYS)
        end_range = now
        return self.filter(
                activated= False,
                forced_expired= False
              ).filter(
                timestamp__gt=start_range,
                timestamp__lte=end_range
              )


class EmailActivationManager(models.Manager):
    def get_queryset(self):
        return EmailActivationQuerySet(self.model, using=self._db)

    def to_be_confirm(self):
        return self.get_queryset().to_be_confirm()

    def email_exists(self, email):
        return self.get_queryset().filter(
                    Q(email=email) |
                    Q(user__email=email)
                ).filter(
                    activated=False
                )


class EmailActivation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField()
    key = models.CharField(max_length=120, blank=True, null=True)
    activated = models.BooleanField(default=False)
    forced_expired = models.BooleanField(default=False)
    expires = models.IntegerField(default=7)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    objects = EmailActivationManager()

    def __str__(self):
        return self.email

    def can_activate(self):
        qs = EmailActivation.objects.filter(pk=self.pk).confirmable()
        if qs.exists():
            return True
        return False

    def activate(self):
        if self.can_activate():
            # pre activation user signal
            user = self.user
            user.is_active = True
            user.save()
            # post activation signal for user
            self.activated = True
            self.save()
            return True
        return False

    def regenerate(self):
        self.key = None
        self.save()
        if self.key is not None:
            return True
        return False

    def send_activation(self):
        if not self.activated and not self.forced_expired:
            if self.key:
                base_url = getattr(settings, 'BASE_URL', 'http://www.flobrr.com')
                key_path = reverse("email-activation", kwargs={'key': self.key})
                path = "{base}{path}".format(base=base_url, path=key_path)
                context = {
                    'path': path,
                    'email': self.email
                }
                txt_ = get_template("registration/emails/verify.txt").render(context)
                html_ = get_template("registration/emails/verify.html").render(context)
                subject = '1-Click Email Verification'
                from_email = settings.DEFAULT_FROM_EMAIL
                recipient_list = [self.email]
                sent_mail = send_mail(
                            subject,
                            txt_,
                            from_email,
                            recipient_list,
                            html_message=html_,
                            fail_silently=False,
                    )
                return sent_mail
        return False


def pre_save_email_activation(sender, instance, *args, **kwargs):
    if not instance.activated and not instance.forced_expired:
        if not instance.key:
            instance.key = unique_key_generator(instance)


pre_save.connect(pre_save_email_activation, sender=EmailActivation)


def post_save_user_create_receiver(sender, instance, created, *args, **kwargs):
    if created:
        obj = EmailActivation.objects.create(user=instance, email=instance.email)
        obj.send_activation()


post_save.connect(post_save_user_create_receiver, sender=User)



