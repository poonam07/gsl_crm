# import redis, datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView, DetailView, CreateView, UpdateView, TemplateView, View
from django.views.generic.edit import FormMixin, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.db.models import Count
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, TrigramSimilarity
# from taggit.models import Tag, TaggedItem
from django.conf import settings

from GSLCRM.mixins import *
from .models import *
from .forms import *
# from .texts import *

# r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)


class LoginView(NextUrlMixin, RequestFormAttachMixin, FormView):
    form_class = LoginForm

    # success_url = 'account:candidate-dashboard'
    template_name = 'registration/login.html'
    default_next = 'login'

    def form_valid(self, form):
        # print(self.form)
        next_path = self.get_next_url()
        return redirect(next_path)


class RegisterView(FormView, CreateView):
    form_class = RegisterForm
    success_url = reverse_lazy('login')
    template_name = 'registration/register.html'

    reference = {}
    key =None
    verification_key =None

    # def get(self, request,  key=None, verification_key=None, *args, **kwargs):
    #     self.key = key
    #     if key is not None:
    #         qs = EmailActivation.objects.filter(key__iexact=key)
    #         active_confirm_qs = qs.confirmable()
    #         if active_confirm_qs.count() == 1:
    #             obj = active_confirm_qs.first()
    #             obj.activate()
    #             messages.success(request, """Congratulations! email confirmed. Please login and update your personal
    #              information""")
    #             return render(request, self.template_name,)
    #
    #         return render(request, self.template_name,)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            request = self.request
            form.save()
            messages.success(request, 'Registration Successful, Please verify your email.')
            return HttpResponseRedirect(self.success_url)

        else:
            messages.warning(request, f'{form.errors}')
            return render(request, self.template_name, {'form': form})


class AccountEmailActivateView(FormMixin, View):
    success_url = reverse_lazy('login')
    form_class = ReactivateEmailForm
    key = None

    def get(self, request, key=None, *args, **kwargs):
        self.key = key
        if key is not None:
            qs = EmailActivation.objects.filter(key__iexact=key)
            confirm_qs = qs.confirmable()
            if confirm_qs.count() == 1:
                obj = confirm_qs.first()
                obj.activate()
                messages.success(request, "Your email has been confirmed. Please login.")
                return redirect(reverse_lazy('login'))
            else:
                activated_qs = qs.filter(activated=True)
                if activated_qs.exists():
                    reset_link = reverse("password_reset")
                    msg = """Your email has already been confirmed
                    Do you need to <a href="{link}">reset your password</a>?
                    """.format(link=reset_link)
                    messages.success(request, mark_safe(msg))
                    return redirect(reverse_lazy('login'))
        context = {'form': self.get_form(),'key': key}
        return render(request, 'registration/activation_error.html', context)

    def post(self, request, *args, **kwargs):
        # create form to receive an email
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        messages.success(self.request, """Activation link sent, please check your email.""")
        email = form.cleaned_data.get("email")
        obj = EmailActivation.objects.email_exists(email).first()
        user = obj.user
        print(obj, user)
        new_activation = EmailActivation.objects.create(user=user, email=email)
        new_activation.send_activation()
        return super(AccountEmailActivateView, self).form_valid(form)

    def form_invalid(self, form):
        context = {'form': form, "key": self.key }
        return render(self.request, 'registration/activation_error.html', context)