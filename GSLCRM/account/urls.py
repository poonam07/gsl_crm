from django.conf.urls import url, include
from django.urls import path
from django.urls import reverse_lazy
from django.contrib.auth import views as auth_views
from . import views

# app_name = 'account'

urlpatterns = [

    path('login/', views.LoginView.as_view(), name='login'),
    path('register/', views.RegisterView.as_view(), name='register'),
    # url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    path('password-change/', auth_views.PasswordChangeView.as_view(
        template_name='registration/password_change.html',
        success_url=reverse_lazy('password_change_done')), name='password_change'),
    path('password-change-done/', auth_views.PasswordChangeDoneView.as_view(
        template_name='registration/password_change_done.html'), name='password_change_done'),
    path('password-reset/', auth_views.PasswordResetView.as_view(
        template_name='registration/password_reset_form1.html',
        success_url=reverse_lazy('password_reset_confirm')), name='password_reset'),
    path('password-reset-done/', auth_views.PasswordResetDoneView.as_view(
        template_name='registration/password_reset_done.html'), name='password_reset_done'),
    # path('password-reset-confirm/', auth_views.PasswordResetConfirmView.as_view(
    #     template_name='registration/password_reset_confirm_ok.html'), name='password_reset_confirm'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='registration/password_reset_email1.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='registration/password_reset_complete.html'), name='password_reset_complete'),

    url(r'^email/confirm/(?P<key>[0-9A-Za-z]+)$', views.AccountEmailActivateView.as_view(),
        name='email-activation'),
    # url(r'^volunteer-profile-verification/(?P<verification_key>[0-9A-Za-z]+)$',
    #     views.VerifyVolunteerProfile.as_view(),
    #     name='volunteer-profile-verification'),
    url(r'^email/resend-activation/$',
        views.AccountEmailActivateView.as_view(),
        name='resend-activation'),

    # url(r'^tag/(?P<id>[\w-]+)$', views.PostListView.as_view(), name='post_list_by_tag'),
    # url(r'^news/(?P<year>[\w-]+)/(?P<month>[\w-]+)/(?P<day>[\w-]+)/(?P<slug>[\w-]+)/$',
    #     views.PostDetailView.as_view(), name='news-detail'),
    url('social', include('social_django.urls', namespace='social')),
]