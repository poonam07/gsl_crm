from rest_framework import serializers
from rest_framework import exceptions
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.db.models import Q

from account.models import User
from contractor.models import (Contractor, ContractorAddress, ContractorService, ContractorLicence, ContractorReview,
                               ContractorShare)
from customer.models import Customer, CustomerAddress, CustomerAccount, CustomerDocument


class LoginSerializer(serializers.ModelSerializer):
    token = serializers.CharField(allow_blank=True, read_only=True)
    email = serializers.EmailField(required=True, label='Email')
    password = serializers.CharField()

    class Meta:
        model = User
        fields = ['email', 'password', 'token']
        extra_kwargs = {"password": {"write_only": True}}

    def validate(self, data):
        user_obj = None
        email = data.get("email", None)
        password = data.get("password", None)

        if not email and not password:
            raise ValidationError("Must provide email and password both.")

        user = User.objects.filter(
            Q(email=email)
        ).distinct()
        if user.exists() and user.count() == 1:
            user.first()
        else:
            raise ValidationError('This Email is not valid')
        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError('Incorrect credentials please try again.')
        data['token'] = 'some random token'

        return data


# class LoginSerializer(serializers.Serializer):
#     email = serializers.CharField()
#     password = serializers.CharField()
#
#     def validate(self, data):
#         email = data.get("email", "")
#         password = data.get("password", "")
#
#         if email and password:
#             user = authenticate(username=email, password=password)
#             if user:
#                 if user.is_active:
#                     data["user"] = user
#                 else:
#                     raise exceptions.ValidationError("User is deactivated.")
#             else:
#                 raise exceptions.ValidationError("Unable to login with given credentials.")
#         else:
#             raise exceptions.ValidationError("Must provide username and password both.")
#         return data


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = User
        fields = ['url', 'email', 'mobile', 'first_name', 'middle_name', 'last_name', 'password', 'password2',
                  'customer', 'vendor', 'supplier', 'contractor', 'sub_contractor', 'project_manager', 'manager',
                  'is_active', 'created']
        extra_kwargs = {
            'password': {'write_only': True}
        }
        lookup_field = 'email'

    def save(self, **kwargs):
        account = User(
            email=self.validated_data['email'],
            mobile=self.validated_data['mobile']
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        if password != password2:
            raise serializers.ValidationError({'password': 'Password must match'})
        account.set_password(password)
        account.save()
        return account


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'email', 'mobile', 'first_name', 'middle_name', 'last_name', 'customer', 'vendor', 'supplier',
#                   'contractor', 'sub_contractor', 'project_manager', 'manager', 'is_active', 'created']


class ContractorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contractor
        fields = '__all__'


class ContractorAddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractorAddress
        fields = '__all__'


class ContractorServiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractorService
        fields = '__all__'


class ContractorLicenceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractorLicence
        fields = '__all__'


class ContractorReviewSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractorReview
        fields = '__all__'


class ContractorShareSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractorShare
        fields = '__all__'


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class CustomerAddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomerAddress
        fields = '__all__'


class CustomerAccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomerAccount
        fields = '__all__'


class CustomerDocumentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomerDocument
        fields = '__all__'


