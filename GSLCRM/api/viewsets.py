from django.urls import path, include
from django.contrib.auth import login as django_login, logout as django_logout
from rest_framework.response import Response
from rest_framework import viewsets, filters, status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.pagination import PageNumberPagination
from rest_framework_tracking.mixins import LoggingMixin

from account.models import User
from .serializers import *


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = LoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            # user = authenticate(request, username=new_data['email'], password=new_data['password'])
            # # user = new_data.validated_data["user"]
            # print(user)
            # print(new_data)
            # django_login(request, new_data)
            # print(new_data)
            # token, created = Token.objects.get_or_create(user=user)
            # new_data.update()
            return Response(new_data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class LoginView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        django_login(request, user)
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key}, status=200)


class LogoutView(APIView):
    authentication_classes = (TokenAuthentication,)

    def post(self, request):
        django_logout(request)
        return Response(status=204)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(admin=False)
    serializer_class = UserSerializer
    # authentication_classes = (TokenAuthentication)
    # permission_classes = (IsAuthenticated)
    pagination_class = PageNumberPagination
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['email', 'mobile']

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    #
    # def partial_update(self, request, *args, **kwargs):
    #     pass

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    #
    # def destroy(self, request, *args, **kwargs):
    #     pass



class ContractorViewSet(viewsets.ModelViewSet):
    queryset = Contractor.objects.all()
    serializer_class = ContractorSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'phone', 'user__email', 'user__mobile']


class ContractorAddressViewSet(viewsets.ModelViewSet):
    queryset = ContractorAddress.objects.all()
    serializer_class = ContractorAddressSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class ContractorServiceViewSet(viewsets.ModelViewSet):
    queryset = ContractorService.objects.all()
    serializer_class = ContractorServiceSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class ContractorLicenceViewSet(viewsets.ModelViewSet):
    queryset = ContractorLicence.objects.all()
    serializer_class = ContractorLicenceSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class ContractorReviewViewSet(viewsets.ModelViewSet):
    queryset = ContractorReview.objects.all()
    serializer_class = ContractorReviewSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class ContractorShareViewSet(viewsets.ModelViewSet):
    queryset = ContractorShare.objects.all()
    serializer_class = ContractorShareSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class CustomerAddressViewSet(viewsets.ModelViewSet):
    queryset = CustomerAddress.objects.all()
    serializer_class = CustomerAddressSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class CustomerAccountViewSet(viewsets.ModelViewSet):
    queryset = CustomerAccount.objects.all()
    serializer_class = CustomerAccountSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']


class CustomerDocumentViewSet(viewsets.ModelViewSet):
    queryset = CustomerDocument.objects.all()
    serializer_class = CustomerDocumentSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['email', 'mobile']




