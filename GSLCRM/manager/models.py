from django.db import models
from account.models import User


class ProjectType(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_type')
    project_type = models.CharField(max_length=250, blank=True, null=True, help_text='Enter your project type')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return str(self.project_type)


class ProjectStatusType(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_status_type')
    project_status_type = models.CharField(max_length=250, blank=True, null=True,
                                           help_text='Enter your project status type')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return str(self.project_status_type)
