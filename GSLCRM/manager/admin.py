from django.contrib import admin

from .models import (ProjectType, ProjectStatusType)

admin.site.register(ProjectType)
admin.site.register(ProjectStatusType)