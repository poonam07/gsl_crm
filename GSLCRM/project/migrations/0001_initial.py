# Generated by Django 2.2 on 2020-06-03 19:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('manager', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('budget', models.PositiveIntegerField(default=0)),
                ('description_of_work', models.TextField(help_text='Your project details - max 50000 words', max_length=50000)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('project_type', models.ManyToManyField(help_text='You can select more than one type.', to='manager.ProjectType')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='projects', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(blank=True, help_text='Your Street Address', max_length=250, null=True)),
                ('street_2', models.CharField(blank=True, help_text='Your Street Address 2', max_length=250, null=True)),
                ('city', models.CharField(blank=True, help_text='Your City', max_length=250, null=True)),
                ('state', models.CharField(blank=True, help_text='Your State', max_length=250, null=True)),
                ('country', models.CharField(blank=True, help_text='Your Country will be shared.', max_length=250, null=True)),
                ('zip', models.PositiveIntegerField(blank=True, help_text='Your zip', null=True)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='location', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.BooleanField(default=False)),
                ('email', models.BooleanField(default=False)),
                ('text', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contact', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
