from django.db import models
from account.models import User

from manager.models import ProjectType


class Project(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='projects')
    project_type = models.ManyToManyField(ProjectType, help_text='You can select more than one type.')
    budget = models.PositiveIntegerField(default=0)
    description_of_work = models.TextField(max_length=50000, help_text='Your project details - max 50000 words')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.full_name)


class Location(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='location')
    street = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address')
    street_2 = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address 2')
    city = models.CharField(max_length=250, blank=True, null=True, help_text='Your City')
    state = models.CharField(max_length=250, blank=True, null=True, help_text='Your State')
    country = models.CharField(max_length=250, blank=True, null=True, help_text='Your Country will be shared.')
    zip = models.PositiveIntegerField(blank=True, null=True, help_text='Your zip')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.full_name)


class Contact(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='contact')
    phone = models.BooleanField(default=False)
    email = models.BooleanField(default=False)
    text = models.BooleanField(default=False)

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.full_name)
