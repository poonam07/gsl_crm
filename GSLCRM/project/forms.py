from django import forms
from django.contrib.auth import authenticate, login, get_user_model

User = get_user_model()


class SearchForm(forms.Form):
    q = forms.CharField()