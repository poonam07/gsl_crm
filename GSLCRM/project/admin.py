from django.contrib import admin

from .models import (Project, Location, Contact)

admin.site.register(Project)
admin.site.register(Location)
admin.site.register(Contact)