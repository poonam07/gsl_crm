from django.contrib import admin

from .models import (Contractor, ContractorAddress, ContractorService, ContractorLicence, ContractorReview,
                     ContractorShare)

admin.site.register(Contractor)
admin.site.register(ContractorAddress)
admin.site.register(ContractorService)
admin.site.register(ContractorLicence)
admin.site.register(ContractorReview)
admin.site.register(ContractorShare)

