from django.db import models
from django.utils.text import slugify
from account.models import User
from django.core.validators import (EmailValidator, RegexValidator, URLValidator)

from customer.models import Customer


class Contractor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='contractors')
    name = models.CharField(max_length=250, null=True, blank=True, help_text='Enter Your Name')
    slug = models.SlugField(max_length=250, unique_for_date='created')
    phone = models.CharField(max_length=10, db_index=True,
                             validators=[RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                                        message='Enter a valid phone number')],unique=True)
    email = models.EmailField(db_index=True, validators=[EmailValidator], unique=True)
    website = models.CharField(max_length=250, blank=True, null=True, validators=[URLValidator],
                               help_text='Enter your website url if available')
    average = models.PositiveIntegerField(blank=True, null=True, help_text='Average project cost')
    description = models.TextField(max_length=1000, help_text='Description - max 1000 words')
    favorites = models.BooleanField(default=False)

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Contractor, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)


class ContractorAddress(models.Model):
    contractor = models.OneToOneField(Contractor, on_delete=models.CASCADE, related_name='contractor_address')
    street = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address')
    street_2 = models.CharField(max_length=250, blank=True, null=True, help_text='Your Street Address 2')
    city = models.CharField(max_length=250, blank=True, null=True, help_text='Your City')
    state = models.CharField(max_length=250, blank=True, null=True, help_text='Your State')
    country = models.CharField(max_length=250, blank=True, null=True, help_text='Your Country will be shared.')
    zip = models.PositiveIntegerField(blank=True, null=True, help_text='Your zip')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.contractor)


class ContractorService(models.Model):
    contractor = models.OneToOneField(Contractor, on_delete=models.CASCADE, related_name='contractor_service')
    service_name = models.CharField(max_length=250, blank=True, null=True, help_text='Your Service Name')
    description = models.TextField(max_length=10000, help_text='Description - max 10000 words')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.service_name)


class ContractorLicence(models.Model):
    contractor = models.OneToOneField(Contractor, on_delete=models.CASCADE, related_name='contractor_licence')
    licence_name = models.CharField(max_length=250, blank=True, null=True, help_text='Your Licence Title')
    description = models.TextField(max_length=10000, help_text='Description - max 10000 words')

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.licence_name)


class ContractorReview(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE, related_name='contractor_reviews')
    customer = models.OneToOneField(Customer, on_delete=models.CASCADE, related_name='customer_review')
    your_review = models.TextField(max_length=1000, help_text='Description - max 1000 words')

    rating = models.PositiveIntegerField(default=0)

    active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return f'Review by {self.contractor} on {self.customer}'


class ContractorShare(models.Model):
    user_from = models.ForeignKey(User, related_name='rel_from_set', on_delete=models.CASCADE)
    user_to = models.ForeignKey(User, related_name='rel_to_set', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f'{self.user_from} follows {self.user_to}'

