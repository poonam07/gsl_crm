# Generated by Django 2.2 on 2020-06-03 19:44

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('customer', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contractor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, help_text='Enter Your Name', max_length=250, null=True)),
                ('slug', models.SlugField(max_length=250, unique_for_date='created')),
                ('phone', models.CharField(db_index=True, max_length=10, unique=True, validators=[django.core.validators.RegexValidator(message='Enter a valid phone number', regex='^\\+?1?\\d{9,15}$')])),
                ('email', models.EmailField(db_index=True, max_length=254, unique=True, validators=[django.core.validators.EmailValidator])),
                ('website', models.CharField(blank=True, help_text='Enter your website url if available', max_length=250, null=True, validators=[django.core.validators.URLValidator])),
                ('average', models.PositiveIntegerField(blank=True, help_text='Average project cost', null=True)),
                ('description', models.TextField(help_text='Description - max 1000 words', max_length=1000)),
                ('favorites', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='contractors', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ContractorShare',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('user_from', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rel_from_set', to=settings.AUTH_USER_MODEL)),
                ('user_to', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rel_to_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='ContractorService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_name', models.CharField(blank=True, help_text='Your Service Name', max_length=250, null=True)),
                ('description', models.TextField(help_text='Description - max 10000 words', max_length=10000)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('contractor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='contractor_service', to='contractor.Contractor')),
            ],
        ),
        migrations.CreateModel(
            name='ContractorReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('your_review', models.TextField(help_text='Description - max 1000 words', max_length=1000)),
                ('rating', models.PositiveIntegerField(default=0)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('contractor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contractor_reviews', to='contractor.Contractor')),
                ('customer', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='customer_review', to='customer.Customer')),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='ContractorLicence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('licence_name', models.CharField(blank=True, help_text='Your Licence Title', max_length=250, null=True)),
                ('description', models.TextField(help_text='Description - max 10000 words', max_length=10000)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('contractor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='contractor_licence', to='contractor.Contractor')),
            ],
        ),
        migrations.CreateModel(
            name='ContractorAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(blank=True, help_text='Your Street Address', max_length=250, null=True)),
                ('street_2', models.CharField(blank=True, help_text='Your Street Address 2', max_length=250, null=True)),
                ('city', models.CharField(blank=True, help_text='Your City', max_length=250, null=True)),
                ('state', models.CharField(blank=True, help_text='Your State', max_length=250, null=True)),
                ('country', models.CharField(blank=True, help_text='Your Country will be shared.', max_length=250, null=True)),
                ('zip', models.PositiveIntegerField(blank=True, help_text='Your zip', null=True)),
                ('active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('contractor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='contractor_address', to='contractor.Contractor')),
            ],
        ),
    ]
